$(document).ready(function(){
    //===========================================登录开始=================================================

    $(document).on('click', '#login_submit', function (){
        var username=$("#aw-login-user-name").val();
        var password=$("#aw-login-user-password").val();
        var redirectUrl=$("#redirectUrl").val();
        console.log(username,password,redirectUrl);
        $.ajax({
            url:'/user/login',
            type:'post',
            data:{'username':username,'password':password,"redirectUrl":redirectUrl},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("登录成功", { shift: -1 }, function () {
                        location.href = data.data;
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });

    $("#login_form").keydown(function(e){
        var e = e || event,
            keycode = e.which || e.keyCode;
        if (keycode==13) {
            $("#login_submit").trigger("click");
        }
    });

});