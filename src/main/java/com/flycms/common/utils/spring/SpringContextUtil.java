package com.flycms.common.utils.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 11:16 2019/11/25
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {

    /**
     * 是否手机访问线程变量
     */
    private static ThreadLocal<Boolean> isMobileHolder = new ThreadLocal<Boolean>();
    private static ThreadLocal<Boolean> isTabletHolder = new ThreadLocal<Boolean>();
    private static ThreadLocal<Boolean> isPcHolder = new ThreadLocal<Boolean>();
    private static ThreadLocal<Boolean> isWxH5Holder = new ThreadLocal<Boolean>();

    // Spring应用上下文环境
    private static ApplicationContext applicationContext;

    public static void setMobile(boolean isMobile) {
        isMobileHolder.set(isMobile);
    }

    public static boolean isMobile() {
        return isMobileHolder.get() != null && isMobileHolder.get();
    }

    public static void resetMobile() {
        isMobileHolder.remove();
    }

    public static void setWxH5(boolean isWxH5) {
        isWxH5Holder.set(isWxH5);
    }

    public static boolean isWxH5() {
        return isWxH5Holder.get() != null && isWxH5Holder.get();
    }

    public static void resetWxH5() {
        isWxH5Holder.remove();
    }

    public static void setPc(boolean isPc) {
        isPcHolder.set(isPc);
    }

    public static boolean isPc() {
        return isPcHolder.get() != null && isPcHolder.get();
    }

    public static void resetPc() {
        isPcHolder.remove();
    }

    public static void setTablet(boolean isTablet) {
        isTabletHolder.set(isTablet);
    }

    public static boolean isTablet() {
        return isTabletHolder.get() != null && isTabletHolder.get();
    }

    public static void resetTablet() {
        isTabletHolder.remove();
    }

    /**
     * 实现ApplicationContextAware接口的回调方法，设置上下文环境
     *
     * @param applicationContext
     */
    public void setApplicationContext(ApplicationContext applicationContext) {
        SpringContextUtil.applicationContext = applicationContext;
    }

    /**
     * @return ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 获取对象
     *
     * @param name
     * @return Object
     * @throws BeansException
     */
    public static Object getBean(String name) throws BeansException {
        return applicationContext.getBean(name);
    }

    /**
     * 通过类型获取对象
     *
     * @param t
     *            对象类型
     * @return
     * @throws BeansException
     */
    public static <T> T getBean(Class<T> t) throws BeansException {
        return applicationContext.getBean(t);
    }
}
