package com.flycms.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
@Slf4j
public class IKAnalyzerUtils {

    /**
     * 分词操作
     *
     * @param txt 关键词
     * @param useSmart 分词方式，true最大分词，false最小分词
     * @return
     */
    public static List<String> cut(String txt,Boolean useSmart) {
        try {
            StringReader sr=new StringReader(txt);
            IKSegmenter ik=new IKSegmenter(sr, useSmart);
            Lexeme lex=null;
            List<String> list=new ArrayList<>();
            while((lex=ik.next())!=null){
                list.add(lex.getLexemeText());
            }
            return list;
        } catch ( IOException e) {
            log.error("分词工具出现错误"+e);
        }
        return null;
    }
}
