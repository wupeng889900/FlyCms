package com.flycms.modules.monitor.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.flycms.common.constant.Constants;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.redis.RedisCache;
import com.flycms.framework.security.LoginAdmin;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.framework.web.page.TableDataInfo;
import com.flycms.modules.system.service.IFlyAdminOnlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.modules.monitor.domain.FlyAdminOnline;

/**
 * 在线用户监控
 * 
 * @author kaifei sun
 */
@RestController
@RequestMapping("/system/monitor/online")
public class FlyUserOnlineController extends BaseController
{
    @Autowired
    private IFlyAdminOnlineService userOnlineService;

    @Autowired
    private RedisCache redisCache;

    @PreAuthorize("@ss.hasPermi('monitor:online:list')")
    @GetMapping("/list")
    public TableDataInfo list(String ipaddr, String userName)
    {
        Collection<String> keys = redisCache.keys(Constants.LOGIN_TOKEN_KEY + "*");
        List<FlyAdminOnline> userOnlineList = new ArrayList<FlyAdminOnline>();
        for (String key : keys)
        {
            LoginAdmin user = redisCache.getCacheObject(key);
            if (StrUtils.isNotEmpty(ipaddr) && StrUtils.isNotEmpty(userName))
            {
                if (StrUtils.equals(ipaddr, user.getIpaddr()) && StrUtils.equals(userName, user.getUsername()))
                {
                    userOnlineList.add(userOnlineService.selectOnlineByInfo(ipaddr, userName, user));
                }
            }
            else if (StrUtils.isNotEmpty(ipaddr))
            {
                if (StrUtils.equals(ipaddr, user.getIpaddr()))
                {
                    userOnlineList.add(userOnlineService.selectOnlineByIpaddr(ipaddr, user));
                }
            }
            else if (StrUtils.isNotEmpty(userName) && StrUtils.isNotNull(user.getAdmin()))
            {
                if (StrUtils.equals(userName, user.getUsername()))
                {
                    userOnlineList.add(userOnlineService.selectOnlineByAdminName(userName, user));
                }
            }
            else
            {
                userOnlineList.add(userOnlineService.loginAdminToAdminOnline(user));
            }
        }
        Collections.reverse(userOnlineList);
        userOnlineList.removeAll(Collections.singleton(null));
        return getDataTable(userOnlineList,10);
    }

    /**
     * 强退用户
     */
    @PreAuthorize("@ss.hasPermi('monitor:online:forceLogout')")
    @Log(title = "在线用户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tokenId}")
    public AjaxResult forceLogout(@PathVariable String tokenId)
    {
        redisCache.deleteObject(Constants.LOGIN_TOKEN_KEY + tokenId);
        return AjaxResult.success();
    }
}
