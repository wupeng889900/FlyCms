package com.flycms.modules.monitor.controller;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.framework.web.page.TableDataInfo;
import com.flycms.modules.monitor.domain.dto.OperLogDTO;
import com.flycms.modules.monitor.service.IFlyOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.flycms.modules.monitor.domain.OperLog;

/**
 * 操作日志记录
 * 
 * @author kaifei sun
 */
@RestController
@RequestMapping("/system/monitor/operlog")
public class FlyOperlogController extends BaseController
{
    @Autowired
    private IFlyOperLogService operLogService;

    /**
     * 查询操作日志记录列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:operLog:list')")
    @GetMapping("/list")
    public TableDataInfo list(OperLog operLog,
                              @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @Sort @RequestParam(defaultValue = "id") String sort,
                              @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<OperLogDTO> pager = operLogService.selectOperLogPager(operLog, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出操作日志记录列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:operLog:export')")
    @Log(title = "操作日志记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OperLog operLog)
    {
        List<OperLogDTO> operLogList = operLogService.exportOperLogList(operLog);
        ExcelUtil<OperLogDTO> util = new ExcelUtil<OperLogDTO>(OperLogDTO.class);
        return util.exportExcel(operLogList, "operLog");
    }

    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/{operIds}")
    public AjaxResult remove(@PathVariable Long[] operIds)
    {
        return toAjax(operLogService.deleteOperLogByIds(operIds));
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/clean")
    public AjaxResult clean()
    {
        operLogService.cleanOperLog();
        return AjaxResult.success();
    }
}
