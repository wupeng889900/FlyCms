package com.flycms.modules.site.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.site.domain.SiteAboutClassify;
import com.flycms.modules.site.domain.dto.SiteAboutClassifyDTO;

import java.util.List;

/**
 * 关于我们分类Service接口
 * 
 * @author admin
 * @date 2021-01-27
 */
public interface ISiteAboutClassifyService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增关于我们分类
     *
     * @param siteAboutClassify 关于我们分类
     * @return 结果
     */
    public int insertSiteAboutClassify(SiteAboutClassify siteAboutClassify);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除关于我们分类
     *
     * @param ids 需要删除的关于我们分类ID
     * @return 结果
     */
    public int deleteSiteAboutClassifyByIds(Long[] ids);

    /**
     * 删除关于我们分类信息
     *
     * @param id 关于我们分类ID
     * @return 结果
     */
    public int deleteSiteAboutClassifyById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改关于我们分类
     *
     * @param siteAboutClassify 关于我们分类
     * @return 结果
     */
    public int updateSiteAboutClassify(SiteAboutClassify siteAboutClassify);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询关于我们分类
     * 
     * @param id 关于我们分类ID
     * @return 关于我们分类
     */
    public SiteAboutClassifyDTO findSiteAboutClassifyById(Long id);

    /**
     * 查询关于我们分类列表
     * 
     * @param siteAboutClassify 关于我们分类
     * @return 关于我们分类集合
     */
    public Pager<SiteAboutClassifyDTO> selectSiteAboutClassifyPager(SiteAboutClassify siteAboutClassify, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的关于我们分类列表
     *
     * @param siteAboutClassify 关于我们分类
     * @return 关于我们分类集合
     */
    public List<SiteAboutClassifyDTO> exportSiteAboutClassifyList(SiteAboutClassify siteAboutClassify);
}
