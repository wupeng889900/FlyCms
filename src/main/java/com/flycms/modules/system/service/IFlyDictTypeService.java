package com.flycms.modules.system.service;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.FlyDictType;
import com.flycms.modules.system.domain.dto.DictTypeQueryDTO;

/**
 * 字典 业务层
 * 
 * @author kaifei sun
 */
public interface IFlyDictTypeService
{
    /**
     * 根据所有字典类型
     * 
     * @return 字典类型集合信息
     */
    public List<FlyDictType> selectDictTypeAll();

    /**
     * 根据字典类型ID查询信息
     * 
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    public FlyDictType selectDictTypeById(Long dictId);

    /**
     * 根据字典类型查询信息
     * 
     * @param dictType 字典类型
     * @return 字典类型
     */
    public FlyDictType selectDictTypeByType(String dictType);

    /**
     * 通过字典ID删除字典信息
     * 
     * @param dictId 字典ID
     * @return 结果
     */
    public int deleteDictTypeById(Long dictId);

    /**
     * 批量删除字典信息
     * 
     * @param dictIds 需要删除的字典ID
     * @return 结果
     */
    public int deleteDictTypeByIds(Long[] dictIds);

    /**
     * 新增保存字典类型信息
     * 
     * @param dictType 字典类型信息
     * @return 结果
     */
    public int insertDictType(FlyDictType dictType);

    /**
     * 修改保存字典类型信息
     * 
     * @param dictType 字典类型信息
     * @return 结果
     */
    public int updateDictType(FlyDictType dictType);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 校验字典类型称是否唯一
     * 
     * @param dictType 字典类型
     * @return 结果
     */
    public String checkDictTypeUnique(FlyDictType dictType);

    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    public List<FlyDictType> selectDictTypeList(FlyDictType dictType);

    /**
     * 根据条件分页查询字典类型
     *
     * @param flyDictType 字典类型
     * @return 字典类型
     */
    public Pager<DictTypeQueryDTO> selectDictTypePager(FlyDictType flyDictType, Integer page, Integer limit, String sort, String order);
}
