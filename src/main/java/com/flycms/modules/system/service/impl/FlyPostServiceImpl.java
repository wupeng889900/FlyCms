package com.flycms.modules.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.exception.CustomException;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.FlyPost;
import com.flycms.modules.system.mapper.FlyAdminPostMapper;
import com.flycms.modules.system.mapper.FlyPostMapper;
import com.flycms.modules.system.domain.dto.PostDTO;
import com.flycms.modules.system.domain.dto.PostQueryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.system.service.IFlyPostService;

/**
 * 岗位信息 服务层处理
 * 
 * @author kaifei sun
 */
@Service
public class FlyPostServiceImpl implements IFlyPostService
{
    @Autowired
    private FlyPostMapper postMapper;

    @Autowired
    private FlyAdminPostMapper adminPostMapper;

    /**
     * 查询所有岗位
     * 
     * @return 岗位列表
     */
    @Override
    public List<PostQueryDTO> selectPostAll()
    {
        List<FlyPost> postList=postMapper.selectPostAll();
        List<PostQueryDTO>  postQueryDTOList = new ArrayList<>();
        postList.forEach(post->{
            PostQueryDTO entity=new PostQueryDTO();
            entity.setPostId(post.getPostId());
            entity.setPostName(post.getPostName());
            entity.setStatus(post.getStatus());
            postQueryDTOList.add(entity);
        });
        return postQueryDTOList;
    }

    /**
     * 通过岗位ID查询岗位信息
     * 
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    @Override
    public FlyPost selectPostById(Long postId)
    {
        return postMapper.selectPostById(postId);
    }

    /**
     * 按任务规则ID查询关联的岗位列表
     *
     * @param ruleId 任务规则ID
     * @return 角色对象信息
     */
    @Override
    public List<FlyPost> selectTaskRulePostByRuleId(Long ruleId)
    {
        return postMapper.selectTaskRulePostByRuleId(ruleId);
    }

    /**
     * 根据用户ID获取岗位选择框列表
     * 
     * @param adminId 用户ID
     * @return 选中岗位ID列表
     */
    public List<Integer> selectPostListByAdminId(Long adminId)
    {
        return postMapper.selectPostListByAdminId(adminId);
    }

    /**
     * 校验岗位名称是否唯一
     * 
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public String checkPostNameUnique(FlyPost post)
    {
        Long postId = StrUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        FlyPost info = postMapper.checkPostNameUnique(post.getPostName());
        if (StrUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验岗位编码是否唯一
     * 
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public String checkPostCodeUnique(FlyPost post)
    {
        Long postId = StrUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        FlyPost info = postMapper.checkPostCodeUnique(post.getPostCode());
        if (StrUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    public int countAdminPostById(Long postId)
    {
        return adminPostMapper.countAdminPostById(postId);
    }

    /**
     * 删除岗位信息
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    public int deletePostById(Long postId)
    {
        return postMapper.deletePostById(postId);
    }

    /**
     * 批量删除岗位信息
     * 
     * @param postIds 需要删除的岗位ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deletePostByIds(Long[] postIds)
    {
        for (Long postId : postIds)
        {
            FlyPost post = selectPostById(postId);
            if (countAdminPostById(postId) > 0)
            {
                throw new CustomException(String.format("%1$s已分配,不能删除", post.getPostName()));
            }
        }
        return postMapper.deletePostByIds(postIds);
    }

    /**
     * 新增保存岗位信息
     * 
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public int insertPost(FlyPost post)
    {
        post.setPostId(SnowFlakeUtils.nextId());
        return postMapper.insertPost(post);
    }

    /**
     * 修改保存岗位信息
     * 
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public int updatePost(FlyPost post)
    {
        return postMapper.updatePost(post);
    }

    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    @Override
    public Pager<PostDTO> selectPostPager(FlyPost post, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<PostDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(post);

        List<FlyPost> postList=postMapper.selectPostPager(pager);
        List<PostDTO> dtolsit = new ArrayList<PostDTO>();
        postList.forEach(entity -> {
            PostDTO taskRuleDto = new PostDTO();
            taskRuleDto.setPostId(entity.getPostId());
            taskRuleDto.setPostName(entity.getPostName());
            taskRuleDto.setPostCode(entity.getPostCode());
            taskRuleDto.setPostSort(entity.getPostSort());
            taskRuleDto.setStatus(entity.getStatus());
            taskRuleDto.setCreateTime(entity.getCreateTime());
            dtolsit.add(taskRuleDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(postMapper.queryPostTotal(pager));
        return pager;
    }
}
