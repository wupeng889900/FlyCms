package com.flycms.modules.system.domain.vo;

import com.flycms.modules.system.domain.FlyDept;
import com.flycms.modules.system.domain.FlyRole;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class AdminVo {
    /** 用户ID */
    private Long adminId;

    /** 部门ID */
    private Long deptId;

    /** 用户账号 */
    private String adminName;

    /** 用户昵称 */
    private String nickName;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phonenumber;

    /** 用户性别 */
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 最后登陆时间 */
    private Date loginDate;

    /** 部门对象 */
    private FlyDept dept;

    /** 角色对象 */
    private List<FlyRole> roles;

    /** 角色组 */
    private Long[] roleIds;

    /** 岗位组 */
    private Long[] postIds;
}
