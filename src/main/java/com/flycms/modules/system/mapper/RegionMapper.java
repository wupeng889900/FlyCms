package com.flycms.modules.system.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.Region;
import org.springframework.stereotype.Repository;

/**
 * 行政区域Mapper接口
 * 
 * @author admin
 * @date 2020-05-31
 */
@Repository
public interface RegionMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增行政区域
     *
     * @param region 行政区域
     * @return 结果
     */
    public int insertRegion(Region region);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除行政区域
     *
     * @param id 行政区域ID
     * @return 结果
     */
    public int deleteRegionById(Long id);

    /**
     * 批量删除行政区域
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRegionByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改行政区域
     *
     * @param region 行政区域
     * @return 结果
     */
    public int updateRegion(Region region);


    /////////////////////////////////
    ///////        查詢       ////////
    /////////////////////////////////
    /**
     * 校验行政区域名称是否唯一
     *
     * @param region 行政区域ID
     * @return 结果
     */
    public int checkRegionRegionNameUnique(Region region);


    /**
     * 查询行政区域
     * 
     * @param id 行政区域ID
     * @return 行政区域
     */
    public Region selectRegionById(Long id);

    /**
     * 查询行政区域数量
     *
     * @param pager 分页处理类
     * @return 行政区域数量
     */
    public int queryRegionTotal(Pager pager);

    /**
     * 查询行政区域列表
     *
     * @param region 行政区域
     * @return 行政区域集合
     */
    public List<Region> selectRegionList(Region region);

}
