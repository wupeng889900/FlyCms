package com.flycms.modules.system.mapper;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.FlyPost;
import org.springframework.stereotype.Repository;

/**
 * 岗位信息 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyPostMapper
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    public int insertPost(FlyPost post);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果
     */
    public int deletePostById(Long postId);

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果
     */
    public int deletePostByIds(Long[] postIds);
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    public int updatePost(FlyPost post);
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询所有岗位
     * 
     * @return 岗位列表
     */
    public List<FlyPost> selectPostAll();

    /**
     * 通过岗位ID查询岗位信息
     * 
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    public FlyPost selectPostById(Long postId);

    /**
     * 按任务规则ID查询关联的岗位列表
     *
     * @param ruleId 任务规则ID
     * @return 角色对象信息
     */
    public List<FlyPost> selectTaskRulePostByRuleId(Long ruleId);
    /**
     * 根据用户ID获取岗位选择框列表
     * 
     * @param adminId 用户ID
     * @return 选中岗位ID列表
     */
    public List<Integer> selectPostListByAdminId(Long adminId);

    /**
     * 查询用户所属岗位组
     * 
     * @param adminName 用户名
     * @return 结果
     */
    public List<FlyPost> selectPostsByAdminName(String adminName);

    /**
     * 校验岗位名称
     * 
     * @param postName 岗位名称
     * @return 结果
     */
    public FlyPost checkPostNameUnique(String postName);

    /**
     * 校验岗位编码
     * 
     * @param postCode 岗位编码
     * @return 结果
     */
    public FlyPost checkPostCodeUnique(String postCode);
    /**
     * 查询岗位数量
     *
     * @param pager 分页处理类
     * @return 岗位数量
     */
    public int queryPostTotal(Pager pager);

    /**
     * 查询岗位数据集合
     *
     * @param pager 分页处理类
     * @return 岗位数据集合
     */
    public List<FlyPost> selectPostPager(Pager pager);
}
