package com.flycms.modules.fullname.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.fullname.mapper.SearchKeywordMapper;
import com.flycms.modules.fullname.domain.SearchKeyword;
import com.flycms.modules.fullname.domain.dto.SearchKeywordDTO;
import com.flycms.modules.fullname.service.ISearchKeywordService;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 姓名搜索Service业务层处理
 * 
 * @author admin
 * @date 2020-10-13
 */
@Service
public class SearchKeywordServiceImpl implements ISearchKeywordService 
{
    @Autowired
    private SearchKeywordMapper searchKeywordMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增姓名搜索
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
    @Override
    public int insertSearchKeyword(SearchKeyword searchKeyword)
    {
        String[] strArr= searchKeyword.getFullName().split("\\n|\\r|↵|、");
        AtomicInteger count = new AtomicInteger();
        HashSet<String> set = new HashSet<>();
        Arrays.asList(strArr).stream().forEach(key ->{
            if(key != null){
                if(!this.SearchKeywordFullNameUnique(key.trim())){
                    searchKeyword.setId(SnowFlakeUtils.nextId());
                    searchKeyword.setFullName(key.trim());
                    searchKeyword.setCreateTime(DateUtils.getNowDate());
                    count.set(searchKeywordMapper.insertSearchKeyword(searchKeyword));
                }else{
                    set.add(key);
                }
            }

        });
        return count.get();
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除姓名搜索
     *
     * @param ids 需要删除的姓名搜索ID
     * @return 结果
     */
    @Override
    public int deleteSearchKeywordByIds(Long[] ids)
    {
        return searchKeywordMapper.deleteSearchKeywordByIds(ids);
    }

    /**
     * 删除姓名搜索信息
     *
     * @param id 姓名搜索ID
     * @return 结果
     */
    @Override
    public int deleteSearchKeywordById(Long id)
    {
        return searchKeywordMapper.deleteSearchKeywordById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓名搜索
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
    @Override
    public int updateSearchKeyword(SearchKeyword searchKeyword)
    {
        return searchKeywordMapper.updateSearchKeyword(searchKeyword);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验姓名是否唯一
     *
     * @param searchKeyword 姓名搜索
     * @return 结果
     */
     @Override
     public String checkSearchKeywordFullNameUnique(SearchKeyword searchKeyword)
     {
         int count = searchKeywordMapper.checkSearchKeywordFullNameUnique(searchKeyword);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }

    /**
     * 查询文章短域名是否被占用
     *
     * @param keyword
     * @return
     */
    public boolean SearchKeywordFullNameUnique(String keyword) {
        SearchKeyword searchKeyword=new SearchKeyword();
        searchKeyword.setFullName(keyword);
        int totalCount = searchKeywordMapper.checkSearchKeywordFullNameUnique(searchKeyword);
        return totalCount > 0 ? true : false;
    }

    /**
     * 查询姓名搜索
     * 
     * @param id 姓名搜索ID
     * @return 姓名搜索
     */
    @Override
    public SearchKeywordDTO findSearchKeywordById(Long id)
    {
            SearchKeyword  searchKeyword=searchKeywordMapper.findSearchKeywordById(id);
        return BeanConvertor.convertBean(searchKeyword,SearchKeywordDTO.class);
    }


    /**
     * 查询姓名搜索列表
     *
     * @param searchKeyword 姓名搜索
     * @return 姓名搜索
     */
    @Override
    public Pager<SearchKeywordDTO> selectSearchKeywordPager(SearchKeyword searchKeyword, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<SearchKeywordDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(searchKeyword);

        List<SearchKeyword> searchKeywordList=searchKeywordMapper.selectSearchKeywordPager(pager);
        List<SearchKeywordDTO> dtolsit = new ArrayList<SearchKeywordDTO>();
        searchKeywordList.forEach(entity -> {
            SearchKeywordDTO dto = new SearchKeywordDTO();
            dto.setId(entity.getId());
            dto.setFullName(entity.getFullName());
            dto.setCountView(entity.getCountView());
            dto.setCreateTime(entity.getCreateTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(searchKeywordMapper.querySearchKeywordTotal(pager));
        return pager;
    }

}
