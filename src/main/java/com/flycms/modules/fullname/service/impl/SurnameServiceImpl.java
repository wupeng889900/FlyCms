package com.flycms.modules.fullname.service.impl;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.fullname.mapper.SurnameMapper;
import com.flycms.modules.fullname.domain.Surname;
import com.flycms.modules.fullname.domain.dto.SurnameDTO;
import com.flycms.modules.fullname.service.ISurnameService;

import java.util.ArrayList;
import java.util.List;

/**
 * 姓氏Service业务层处理
 * 
 * @author admin
 * @date 2020-10-13
 */
@Service
public class SurnameServiceImpl implements ISurnameService 
{
    @Autowired
    private SurnameMapper surnameMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增姓氏
     *
     * @param surname 姓氏
     * @return 结果
     */
    @Override
    public int insertSurname(Surname surname)
    {
        surname.setId(SnowFlakeUtils.nextId());
        return surnameMapper.insertSurname(surname);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除姓氏
     *
     * @param ids 需要删除的姓氏ID
     * @return 结果
     */
    @Override
    public int deleteSurnameByIds(Long[] ids)
    {
        return surnameMapper.deleteSurnameByIds(ids);
    }

    /**
     * 删除姓氏信息
     *
     * @param id 姓氏ID
     * @return 结果
     */
    @Override
    public int deleteSurnameById(Long id)
    {
        return surnameMapper.deleteSurnameById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓氏
     *
     * @param surname 姓氏
     * @return 结果
     */
    @Override
    public int updateSurname(Surname surname)
    {
        return surnameMapper.updateSurname(surname);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验姓氏是否唯一
     *
     * @param surname 姓氏
     * @return 结果
     */
     @Override
     public String checkSurnameLastNameUnique(Surname surname)
     {
         int count = surnameMapper.checkSurnameLastNameUnique(surname);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }


    /**
     * 查询姓氏
     * 
     * @param id 姓氏ID
     * @return 姓氏
     */
    @Override
    public SurnameDTO findSurnameById(Long id)
    {
            Surname  surname=surnameMapper.findSurnameById(id);
        return BeanConvertor.convertBean(surname,SurnameDTO.class);
    }


    /**
     * 查询姓氏列表
     *
     * @param surname 姓氏
     * @return 姓氏
     */
    @Override
    public Pager<SurnameDTO> selectSurnamePager(Surname surname, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<SurnameDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(surname);

        List<Surname> surnameList=surnameMapper.selectSurnamePager(pager);
        List<SurnameDTO> dtolsit = new ArrayList<SurnameDTO>();
        surnameList.forEach(entity -> {
            SurnameDTO dto = new SurnameDTO();
            dto.setId(entity.getId());
            dto.setLastName(entity.getLastName());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(surnameMapper.querySurnameTotal(pager));
        return pager;
    }

}
