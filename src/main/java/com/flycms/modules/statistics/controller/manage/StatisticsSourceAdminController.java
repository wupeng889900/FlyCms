package com.flycms.modules.statistics.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.statistics.domain.StatisticsSource;
import com.flycms.modules.statistics.domain.dto.StatisticsSourceDTO;
import com.flycms.modules.statistics.service.IStatisticsSourceService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 来源统计Controller
 * 
 * @author admin
 * @date 2020-12-09
 */
@RestController
@RequestMapping("/system/statistics/statisticsSource")
public class StatisticsSourceAdminController extends BaseController
{
    @Autowired
    private IStatisticsSourceService statisticsSourceService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增来源统计
     */
    @PreAuthorize("@ss.hasPermi('statistics:statisticsSource:add')")
    @Log(title = "来源统计", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StatisticsSource statisticsSource)
    {
        return toAjax(statisticsSourceService.insertStatisticsSource(statisticsSource));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除来源统计
     */
    @PreAuthorize("@ss.hasPermi('statistics:statisticsSource:remove')")
    @Log(title = "来源统计", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(statisticsSourceService.deleteStatisticsSourceByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改来源统计
     */
    @PreAuthorize("@ss.hasPermi('statistics:statisticsSource:edit')")
    @Log(title = "来源统计", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StatisticsSource statisticsSource)
    {
        return toAjax(statisticsSourceService.updateStatisticsSource(statisticsSource));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询来源统计列表
     */
    @PreAuthorize("@ss.hasPermi('statistics:statisticsSource:list')")
    @GetMapping("/list")
    public TableDataInfo list(StatisticsSource statisticsSource,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<StatisticsSourceDTO> pager = statisticsSourceService.selectStatisticsSourcePager(statisticsSource, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出来源统计列表
     */
    @PreAuthorize("@ss.hasPermi('statistics:statisticsSource:export')")
    @Log(title = "来源统计", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StatisticsSource statisticsSource)
    {
        List<StatisticsSourceDTO> statisticsSourceList = statisticsSourceService.exportStatisticsSourceList(statisticsSource);
        ExcelUtil<StatisticsSourceDTO> util = new ExcelUtil<StatisticsSourceDTO>(StatisticsSourceDTO.class);
        return util.exportExcel(statisticsSourceList, "statisticsSource");
    }

    /**
     * 获取来源统计详细信息
     */
    @PreAuthorize("@ss.hasPermi('statistics:statisticsSource:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(statisticsSourceService.findStatisticsSourceById(id));
    }

}
