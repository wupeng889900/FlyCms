package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.group.mapper.GroupColumnMapper;
import com.flycms.modules.group.domain.GroupColumn;
import com.flycms.modules.group.domain.dto.GroupColumnDTO;
import com.flycms.modules.group.service.IGroupColumnService;

import java.util.List;

/**
 * 小组分类Service业务层处理
 * 
 * @author admin
 * @date 2020-09-25
 */
@Service
public class GroupColumnServiceImpl implements IGroupColumnService 
{
    @Autowired
    private GroupColumnMapper groupColumnMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组分类
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
    @Override
    public int insertGroupColumn(GroupColumn groupColumn)
    {
        groupColumn.setId(SnowFlakeUtils.nextId());
        groupColumn.setUpdateTime(DateUtils.getNowDate());
        groupColumn.setCreateTime(DateUtils.getNowDate());
        return groupColumnMapper.insertGroupColumn(groupColumn);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组分类
     *
     * @param ids 需要删除的小组分类ID
     * @return 结果
     */
    @Override
    public int deleteGroupColumnByIds(Long[] ids)
    {
        return groupColumnMapper.deleteGroupColumnByIds(ids);
    }

    /**
     * 删除小组分类信息
     *
     * @param id 小组分类ID
     * @return 结果
     */
    @Override
    public int deleteGroupColumnById(Long id)
    {
        return groupColumnMapper.deleteGroupColumnById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组分类
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
    @Override
    public int updateGroupColumn(GroupColumn groupColumn)
    {
        groupColumn.setUpdateTime(DateUtils.getNowDate());
        return groupColumnMapper.updateGroupColumn(groupColumn);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验分类名称是否唯一
     *
     * @param groupColumn 小组分类
     * @return 结果
     */
     @Override
     public String checkGroupColumnColumnNameUnique(GroupColumn groupColumn)
     {
         int count = groupColumnMapper.checkGroupColumnColumnNameUnique(groupColumn);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }


    /**
     * 查询小组分类
     * 
     * @param id 小组分类ID
     * @return 小组分类
     */
    @Override
    public GroupColumnDTO findGroupColumnById(Long id)
    {
        GroupColumn column=groupColumnMapper.findGroupColumnById(id);
        return BeanConvertor.convertBean(column,GroupColumnDTO.class);
    }


    /**
    * 查询小组分类所有列表
    *
    * @param groupColumn 小组分类
    * @return 小组分类*/
    @Override
    public List<GroupColumnDTO> selectGroupColumnList(GroupColumn groupColumn)
    {
        List<GroupColumn> column=groupColumnMapper.selectGroupColumnList(groupColumn);
        return BeanConvertor.copyList(column,GroupColumnDTO.class);
    }

}
