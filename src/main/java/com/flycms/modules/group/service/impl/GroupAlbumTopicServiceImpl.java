package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.group.mapper.GroupAlbumTopicMapper;
import com.flycms.modules.group.domain.GroupAlbumTopic;
import com.flycms.modules.group.domain.dto.GroupAlbumTopicDTO;
import com.flycms.modules.group.service.IGroupAlbumTopicService;

import java.util.ArrayList;
import java.util.List;

/**
 * 小组专辑帖子关联Service业务层处理
 * 
 * @author admin
 * @date 2020-12-16
 */
@Service
public class GroupAlbumTopicServiceImpl implements IGroupAlbumTopicService 
{
    @Autowired
    private GroupAlbumTopicMapper groupAlbumTopicMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组专辑帖子关联
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 结果
     */
    @Override
    public int insertGroupAlbumTopic(GroupAlbumTopic groupAlbumTopic)
    {
        groupAlbumTopic.setId(SnowFlakeUtils.nextId());
        groupAlbumTopic.setCreateTime(DateUtils.getNowDate());
        return groupAlbumTopicMapper.insertGroupAlbumTopic(groupAlbumTopic);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组专辑帖子关联
     *
     * @param ids 需要删除的小组专辑帖子关联ID
     * @return 结果
     */
    @Override
    public int deleteGroupAlbumTopicByIds(Long[] ids)
    {
        return groupAlbumTopicMapper.deleteGroupAlbumTopicByIds(ids);
    }

    /**
     * 删除小组专辑帖子关联信息
     *
     * @param id 小组专辑帖子关联ID
     * @return 结果
     */
    @Override
    public int deleteGroupAlbumTopicById(Long id)
    {
        return groupAlbumTopicMapper.deleteGroupAlbumTopicById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组专辑帖子关联
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 结果
     */
    @Override
    public int updateGroupAlbumTopic(GroupAlbumTopic groupAlbumTopic)
    {
        return groupAlbumTopicMapper.updateGroupAlbumTopic(groupAlbumTopic);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询小组专辑帖子关联
     * 
     * @param id 小组专辑帖子关联ID
     * @return 小组专辑帖子关联
     */
    @Override
    public GroupAlbumTopicDTO findGroupAlbumTopicById(Long id)
    {
        GroupAlbumTopic groupAlbumTopic=groupAlbumTopicMapper.findGroupAlbumTopicById(id);
        return BeanConvertor.convertBean(groupAlbumTopic,GroupAlbumTopicDTO.class);
    }


    /**
     * 查询小组专辑帖子关联列表
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 小组专辑帖子关联
     */
    @Override
    public Pager<GroupAlbumTopicDTO> selectGroupAlbumTopicPager(GroupAlbumTopic groupAlbumTopic, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupAlbumTopicDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupAlbumTopic);

        List<GroupAlbumTopic> groupAlbumTopicList=groupAlbumTopicMapper.selectGroupAlbumTopicPager(pager);
        List<GroupAlbumTopicDTO> dtolsit = new ArrayList<GroupAlbumTopicDTO>();
        groupAlbumTopicList.forEach(entity -> {
            GroupAlbumTopicDTO dto = new GroupAlbumTopicDTO();
            dto.setId(entity.getId());
            dto.setAlbumId(entity.getAlbumId());
            dto.setTopicId(entity.getTopicId());
            dto.setChapter(entity.getChapter());
            dto.setSortOrder(entity.getSortOrder());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupAlbumTopicMapper.queryGroupAlbumTopicTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的小组专辑帖子关联列表
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 小组专辑帖子关联集合
     */
    @Override
    public List<GroupAlbumTopicDTO> exportGroupAlbumTopicList(GroupAlbumTopic groupAlbumTopic) {
        return BeanConvertor.copyList(groupAlbumTopicMapper.exportGroupAlbumTopicList(groupAlbumTopic),GroupAlbumTopicDTO.class);
    }
}
