package com.flycms.modules.group.directive;

import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupColumn;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.dto.GroupColumnDTO;
import com.flycms.modules.group.service.IGroupColumnService;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GroupDirective extends BaseTag {

    @Autowired
    private IGroupService groupService;;
    @Autowired
    private IGroupUserService groupUserService;
    @Autowired
    private IGroupColumnService groupColumnService;

    public GroupDirective() {
        super(GroupDirective.class.getName());
    }

    /**
     * 小组列表
     *
     * @param params
     * @return
     */
    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        Long columnId = getLongParam(params, "columnId");
        int status = getStatusParam(params,"status");
        String title = getParam(params, "title");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        Group group = new Group();
        group.setColumnId(columnId);
        group.setGroupName(title);
        group.setStatus(status);
        return groupService.selectGroupPager(group, pageNum, pageSize, sort, order);
    }

    /**
     * 话题分类列表
     *
     * @param params
     * @return
     */
    public Object column(Map params) {
        Long columnId = getLongParam(params, "columnId");
        GroupColumn column = new GroupColumn();
        column.setParentId(columnId);
        return groupColumnService.selectGroupColumnList(column);
    }

    /**
     * 小组分类详细信息
     *
     * @param params
     * @return
     */
    public Object columninfo(Map params) {
        Long id = getLongParam(params, "id");
        GroupColumnDTO column = null;
        if(id != null){
            column = groupColumnService.findGroupColumnById(id);
        }
        return column;
    }

    /**
     * 小组详细信息
     *
     * @param params
     * @return
     */
    public Object groupInfo(Map params) {
        Long id = getLongParam(params, "id");
        Group group = null;
        if(id != null){
            group = groupService.findGroupById(id);
        }
        return group;
    }

    /**
     * 用户关联小组列表
     *
     * @param params
     * @return
     */
    public Object groupOrUser(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);

        Long userId = getLongParam(params, "userId");
        Long groupId = getLongParam(params, "groupId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int status = getStatusParam(params,"status");
        int isadmin = getParamInt(params, "isadmin");
        if(userId == null && groupId == null){
            return null;
        }
        GroupUser groupUser = new GroupUser();
        groupUser.setUserId(userId);
        groupUser.setGroupId(groupId);
        groupUser.setIsadmin(isadmin);
        groupUser.setStatus(status);
        return groupUserService.selectGroupUserPager(groupUser, pageNum, pageSize, sort, order);
    }

    /**
     * 查询用户是否是管理员
     *
     * @param params
     * @return
     */
    public Object admin(Map params) {
        Long userId = getLongParam(params, "userId");
        Long groupId = getLongParam(params, "groupId");
        Integer isadmin = getParamInt(params, "isadmin");
        Integer isfounder = getParamInt(params, "isfounder");
        Integer status = getStatusParam(params,"status");
        GroupUser admin = null;
        if(userId != null || groupId != null){
            GroupUser groupUser = new GroupUser();
            groupUser.setGroupId(groupId);
            groupUser.setUserId(userId);
            groupUser.setIsadmin(isadmin);
            groupUser.setIsfounder(isfounder);
            groupUser.setStatus(status);
            admin = groupUserService.findGroupUser(groupUser);
        }
        return admin;
    }
}
