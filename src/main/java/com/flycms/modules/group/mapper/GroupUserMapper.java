package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupUser;
import org.springframework.stereotype.Repository;

/**
 * 群组和用户对应关系Mapper接口
 * 
 * @author admin
 * @date 2020-09-27
 */
@Repository
public interface GroupUserMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增群组和用户对应关系
     *
     * @param groupUser 群组和用户对应关系
     * @return 结果
     */
    public int insertGroupUser(GroupUser groupUser);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除群组和用户对应关系
     *
     * @param userId 群组和用户对应关系ID
     * @return 结果
     */
    public int deleteGroupUserById(Long userId,Long groupId);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组和用户对应关系
     *
     * @param groupUser 群组和用户对应关系
     * @return 结果
     */
    public int updateGroupUser(GroupUser groupUser);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 按用户ID和群组ID查询关系详细信息
     * 
     * @param groupUser 群组和用户对应关系
     * @return 群组和用户对应关系
     */
    public GroupUser findGroupUser(GroupUser groupUser);

    /**
     * 查询群组和用户对应关系数量
     *
     * @param pager 分页处理类
     * @return 群组和用户对应关系数量
     */
    public int queryGroupUserTotal(Pager pager);

    /**
     * 查询群组和用户对应关系列表
     * 
     * @param pager 分页处理类
     * @return 群组和用户对应关系集合
     */
    public List<GroupUser> selectGroupUserPager(Pager pager);

}
