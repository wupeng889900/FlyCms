package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.Group;
import org.springframework.stereotype.Repository;

/**
 * 群组(小组)Mapper接口
 * 
 * @author admin
 * @date 2020-09-25
 */
@Repository
public interface GroupMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增群组(小组)
     *
     * @param group 群组(小组)
     * @return 结果
     */
    public int insertGroup(Group group);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除群组(小组)
     *
     * @param id 群组(小组)ID
     * @return 结果
     */
    public int deleteGroupById(Long id);

    /**
     * 批量删除群组(小组)
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组(小组)
     *
     * @param group 群组(小组)
     * @return 结果
     */
    public int updateGroup(Group group);

    /**
     * 更新小组内发布话题数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    public int updateCountTopic(Long id);

    /**
     * 更新小组加入人数数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    public int updateCountUser(Long id);

    /**
     * 更新小组回帖数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    public int updateCountComment(Long id);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验群组名称是否唯一
     *
     * @param group 群组(小组)ID
     * @return 结果
     */
    public int checkGroupGroupNameUnique(Group group);

    /**
     * 查询群组(小组)短域名是否被占用
     *
     * @param shortUrl
     * @return
     */
    public int checkGroupShorturlUnique(String shortUrl);

    /**
     * 查询群组(小组)
     * 
     * @param id 群组(小组)ID
     * @return 群组(小组)
     */
    public Group findGroupById(Long id);

    /**
     * 按shortUrl查询群组(小组)信息
     *
     * @param shortUrl
     * @return
     */
    public Group findGroupByShorturl(String shortUrl);

    /**
     * 查询群组(小组)数量
     *
     * @param pager 分页处理类
     * @return 群组(小组)数量
     */
    public int queryGroupTotal(Pager pager);

    /**
     * 查询群组(小组)列表
     * 
     * @param pager 分页处理类
     * @return 群组(小组)集合
     */
    public List<Group> selectGroupPager(Pager pager);

}
