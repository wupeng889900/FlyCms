package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupUserIsaudit;
import org.springframework.stereotype.Repository;

/**
 * 小组成员审核Mapper接口
 * 
 * @author admin
 * @date 2020-11-24
 */
@Repository
public interface GroupUserIsauditMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组成员审核
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 结果
     */
    public int insertGroupUserIsaudit(GroupUserIsaudit groupUserIsaudit);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组成员审核
     *
     * @param userId 小组成员审核ID
     * @return 结果
     */
    public int deleteGroupUserIsauditById(Long userId);

    /**
     * 批量删除小组成员审核
     *
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupUserIsauditByIds(Long[] userIds);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组成员审核
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 结果
     */
    public int updateGroupUserIsaudit(GroupUserIsaudit groupUserIsaudit);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询小组成员审核
     * 
     * @param userId 小组成员审核ID
     * @return 小组成员审核
     */
    public GroupUserIsaudit findGroupUserIsauditById(Long userId,Long groupId);

    /**
     * 查询小组成员审核数量
     *
     * @param pager 分页处理类
     * @return 小组成员审核数量
     */
    public int queryGroupUserIsauditTotal(Pager pager);

    /**
     * 查询小组成员审核列表
     * 
     * @param pager 分页处理类
     * @return 小组成员审核集合
     */
    public List<GroupUserIsaudit> selectGroupUserIsauditPager(Pager pager);

    /**
     * 查询需要导出的小组成员审核列表
     *
     * @param groupUserIsaudit 小组成员审核
     * @return 小组成员审核集合
     */
    public List<GroupUserIsaudit> exportGroupUserIsauditList(GroupUserIsaudit groupUserIsaudit);
}
