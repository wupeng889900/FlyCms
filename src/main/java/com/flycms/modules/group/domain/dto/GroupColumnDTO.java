package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 小组分类数据传输对象 fly_group_column
 * 
 * @author admin
 * @date 2020-09-25
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupColumnDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 分类ID */
    @Excel(name = "分类ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 父分类ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;
    /** 分类名称 */
    @Excel(name = "分类名称")
    private String columnName;
    /** 群组个数 */
    @Excel(name = "群组个数")
    private int countGroup;
    /** 创建时间 */
    @Excel(name = "添加时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 更新时间 */
    @Excel(name = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
