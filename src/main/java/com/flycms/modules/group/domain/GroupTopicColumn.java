package com.flycms.modules.group.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 帖子分类对象 fly_group_topic_column
 * 
 * @author admin
 * @date 2020-11-03
 */
@Data
public class GroupTopicColumn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分类ID */
    private Long id;
    /** 小组ID */
    private Long groupId;
    /** 短域名 */
    private String shortUrl;
    /** 用户ID */
    private Long userId;
    /** 分类名称 */
    private String columnName;
    /** 排序 */
    private Integer sortOrder;
    /** 统计帖子 */
    private Long countTopic;
    /** 显示状态，0不显示，1显示 */
    private String isshow;
    /** 审核状态 */
    private int status;
}
