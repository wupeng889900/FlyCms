package com.flycms.modules.elastic.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.elastic.mapper.SynonymMapper;
import com.flycms.modules.elastic.domain.Synonym;
import com.flycms.modules.elastic.domain.dto.SynonymDTO;
import com.flycms.modules.elastic.service.ISynonymService;

import java.util.ArrayList;
import java.util.List;

/**
 * 同义词词库Service业务层处理
 * 
 * @author admin
 * @date 2020-10-22
 */
@Service
public class SynonymServiceImpl implements ISynonymService 
{
    @Autowired
    private SynonymMapper synonymMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增同义词词库
     *
     * @param synonym 同义词词库
     * @return 结果
     */
    @Override
    public int insertSynonym(Synonym synonym)
    {
        synonym.setId(SnowFlakeUtils.nextId());
        synonym.setCreateTime(DateUtils.getNowDate());
        return synonymMapper.insertSynonym(synonym);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除同义词词库
     *
     * @param ids 需要删除的同义词词库ID
     * @return 结果
     */
    @Override
    public int deleteSynonymByIds(Long[] ids)
    {
        return synonymMapper.deleteSynonymByIds(ids);
    }

    /**
     * 删除同义词词库信息
     *
     * @param id 同义词词库ID
     * @return 结果
     */
    @Override
    public int deleteSynonymById(Long id)
    {
        return synonymMapper.deleteSynonymById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改同义词词库
     *
     * @param synonym 同义词词库
     * @return 结果
     */
    @Override
    public int updateSynonym(Synonym synonym)
    {
        return synonymMapper.updateSynonym(synonym);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验同义词是否唯一
     *
     * @param synonym 同义词词库
     * @return 结果
     */
     @Override
     public String checkSynonymSynonymWordUnique(Synonym synonym)
     {
         int count = synonymMapper.checkSynonymSynonymWordUnique(synonym);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }


    /**
     * 查询同义词词库
     * 
     * @param id 同义词词库ID
     * @return 同义词词库
     */
    @Override
    public SynonymDTO findSynonymById(Long id)
    {
            Synonym  synonym=synonymMapper.findSynonymById(id);
        return BeanConvertor.convertBean(synonym,SynonymDTO.class);
    }


    /**
     * 查询同义词词库列表
     *
     * @param synonym 同义词词库
     * @return 同义词词库
     */
    @Override
    public Pager<SynonymDTO> selectSynonymPager(Synonym synonym, Integer page, Integer limit, String sort, String order)
    {
        Pager<SynonymDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(synonym);

        List<Synonym> synonymList=synonymMapper.selectSynonymPager(pager);
        List<SynonymDTO> dtolsit = new ArrayList<SynonymDTO>();
        synonymList.forEach(entity -> {
            SynonymDTO dto = new SynonymDTO();
            dto.setId(entity.getId());
            dto.setSynonymWord(entity.getSynonymWord());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(synonymMapper.querySynonymTotal(pager));
        return pager;
    }

    /**
     * 查询所有同义词词库列表
     *
     * @return
     */
    @Override
    public List<Synonym> selectSynonymList() {
        return synonymMapper.selectSynonymList();
    }

}
