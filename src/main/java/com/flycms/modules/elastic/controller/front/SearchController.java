package com.flycms.modules.elastic.controller.front;

import com.flycms.common.utils.AgentUtils;
import com.flycms.common.validator.Sort;
import com.flycms.framework.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchController extends BaseController {



    /**
     * 搜索列表
     */
    @GetMapping(value = {"/search"})
    public String listIndex(@RequestParam(value = "q", required = false) String q,
                            @PathVariable(value = "p", required = false) String p,
                       ModelMap modelMap)
    {
        if(p == null) p = "1";
        modelMap.addAttribute("title", q);
        modelMap.addAttribute("p", p);
        if(AgentUtils.judgeIsPhone(request)){
            return theme.getMobileTemplate("search/index");
        }
        return theme.getPcTemplate("search/index");
    }

    /**
     * 搜索翻页列表
     */
    @GetMapping(value = {"/search/{q}-p-{p:\\d+}"})
    public String list(@PathVariable(value = "q", required = false) String q,
                       @PathVariable(value = "p", required = false) String p,
                       ModelMap modelMap)
    {
        if(p == null) p = "1";
        modelMap.addAttribute("title", q);
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("search/index");
    }
}
