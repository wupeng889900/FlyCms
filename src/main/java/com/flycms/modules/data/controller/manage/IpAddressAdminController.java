package com.flycms.modules.data.controller.manage;


import com.flycms.common.utils.ip.IpUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import com.flycms.modules.data.domain.vo.IpAddressVO;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.data.domain.IpAddress;
import com.flycms.modules.data.domain.dto.IpAddressDTO;
import com.flycms.modules.data.service.IIpAddressService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * IP地址库Controller
 * 
 * @author admin
 * @date 2020-12-10
 */
@RestController
@RequestMapping("/system/data/ipAddress")
public class IpAddressAdminController extends BaseController
{
    @Autowired
    private IIpAddressService ipAddressService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增IP地址库
     */
    @PreAuthorize("@ss.hasPermi('tool:ipAddress:add')")
    @Log(title = "IP地址库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IpAddressVO vo)
    {
        Long startip=IpUtils.ip2Long(vo.getStartIp());
        if (16777215 >= startip.longValue())
        {
            return AjaxResult.error("该ip为保留地址，无需添加！");
        }
        return toAjax(ipAddressService.insertIpAddress(vo));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除IP地址库
     */
    @PreAuthorize("@ss.hasPermi('tool:ipAddress:remove')")
    @Log(title = "IP地址库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ipAddressService.deleteIpAddressByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改IP地址库
     */
    @PreAuthorize("@ss.hasPermi('tool:ipAddress:edit')")
    @Log(title = "IP地址库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IpAddressVO vo)
    {
        Long startip=IpUtils.ip2Long(vo.getStartIp());
        if (16777215 >= startip.longValue())
        {
            return AjaxResult.error("该ip为保留地址，无需添加！");
        }
        return toAjax(ipAddressService.updateIpAddress(vo));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询IP地址库列表
     */
    @PreAuthorize("@ss.hasPermi('tool:ipAddress:list')")
    @GetMapping("/list")
    public TableDataInfo list(IpAddress ipAddress,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<IpAddressDTO> pager = ipAddressService.selectIpAddressPager(ipAddress, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出IP地址库列表
     */
    @PreAuthorize("@ss.hasPermi('tool:ipAddress:export')")
    @Log(title = "IP地址库", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(IpAddress ipAddress)
    {
        List<IpAddressDTO> ipAddressList = ipAddressService.exportIpAddressList(ipAddress);
        ExcelUtil<IpAddressDTO> util = new ExcelUtil<IpAddressDTO>(IpAddressDTO.class);
        return util.exportExcel(ipAddressList, "ipAddress");
    }

    /**
     * 获取IP地址库详细信息
     */
    @PreAuthorize("@ss.hasPermi('tool:ipAddress:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ipAddressService.findIpAddressById(id));
    }

}
