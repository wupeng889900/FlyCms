package com.flycms.modules.data.domain.vo;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * IP地址库对象 fly_ip_address
 * 
 * @author admin
 * @date 2020-12-10
 */
@Data
public class IpAddressVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** IP段起始 */
    private String startIp;
    /** IP段结束 */
    private String endIp;
    /** 国家 */
    private String country;
    /** 省份 */
    private String province;
    /** 市级地区 */
    private String city;
    /** 县 */
    private String county;
    /** 详细地址 */
    private String address;
    /** 详细地址 */
    private String local;
}
