package com.flycms.modules.data.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.LabelFollow;
import org.springframework.stereotype.Repository;

/**
 * 话题关注Mapper接口
 * 
 * @author admin
 * @date 2021-02-01
 */
@Repository
public interface LabelFollowMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题关注
     *
     * @param labelFollow 话题关注
     * @return 结果
     */
    public int insertLabelFollow(LabelFollow labelFollow);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除话题关注
     *
     * @param id 话题关注ID
     * @return 结果
     */
    public int deleteLabelFollowById(Long id);

    /**
     * 批量删除话题关注
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteLabelFollowByIds(Long[] ids);

    /**
     * 删除用户和标签关联
     *
     * @param labelId 标签id
     * @param userId 用户id
     * @return
     */
    public int deleteLabelFollow(Long labelId,Long userId);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     *
     * @param labelFollow 话题关注
     * @return 结果
     */
    public int updateLabelFollow(LabelFollow labelFollow);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 检查是否已关注该标签
     *
     * @param labelId 标签id
     * @param userId 用户id
     * @return
     */
    public int checkLabelFollow(Long labelId,Long userId);

    /**
     * 查询话题关注
     * 
     * @param id 话题关注ID
     * @return 话题关注
     */
    public LabelFollow findLabelFollowById(Long id);

    /**
     * 查询话题关注数量
     *
     * @param pager 分页处理类
     * @return 话题关注数量
     */
    public int queryLabelFollowTotal(Pager pager);

    /**
     * 查询话题关注列表
     * 
     * @param pager 分页处理类
     * @return 话题关注集合
     */
    public List<LabelFollow> selectLabelFollowPager(Pager pager);

    /**
     * 查询需要导出的话题关注列表
     *
     * @param labelFollow 话题关注
     * @return 话题关注集合
     */
    public List<LabelFollow> exportLabelFollowList(LabelFollow labelFollow);
}
