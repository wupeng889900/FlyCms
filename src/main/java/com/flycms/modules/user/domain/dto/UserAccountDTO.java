package com.flycms.modules.user.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户账户数据传输对象 fly_user_account
 * 
 * @author admin
 * @date 2020-12-04
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserAccountDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    @Excel(name = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 用户余额 */
    @Excel(name = "用户余额")
    private Double balance;
    /** 积分 */
    @Excel(name = "积分")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer score;
    /** 经验值 */
    @Excel(name = "经验值")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer exp;
    /** 话题数量 */
    @Excel(name = "话题数量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countTopic;
    /** 加入小组数 */
    @Excel(name = "加入小组数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countGroup;
    /** 粉丝数 */
    @Excel(name = "粉丝数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countFans;
    /** 被注数 */
    @Excel(name = "被注数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countFollow;

}
