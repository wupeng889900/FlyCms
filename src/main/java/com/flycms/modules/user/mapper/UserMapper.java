package com.flycms.modules.user.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 用户Mapper接口
 * 
 * @author kaifei sun
 * @date 2020-10-30
 */
@Repository
public interface UserMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户
     *
     * @param user 用户
     * @return 结果
     */
    public int insertUser(User user);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户
     *
     * @param id 用户ID
     * @return 结果
     */
    public int deleteUserById(Long id);

    /**
     * 批量删除用户
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户
     *
     * @param user 用户
     * @return 结果
     */
    public int updateUser(User user);

    /**
     * 修改用户头像
     *
     * @param id 用户id
     * @param avatar  头像地址
     * @return
     */
    public int updateUserAvatar(Long id,String avatar);

    /**
     * 修改用户头像
     *
     * @param id 用户id
     * @param password  用户密码
     * @return
     */
    public int updatePassword(Long id,String password);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验账号是否唯一
     *
     * @param user 用户ID
     * @return 结果
     */
    public int checkUsernameUnique(User user);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户ID
     * @return 结果
     */
    public int checkUserMobileUnique(User user);

    /**
     * 校验邮箱是否唯一
     *
     * @param user 用户ID
     * @return 结果
     */
    public int checkUserEmailUnique(User user);

    /**
     * 查询用户管理短域名是否被占用
     *
     * @param shortUrl 短域名
     * @return
     */
    public int checkUserShorturlUnique(String shortUrl);

    /**
     * 按shortUrl查询用户信息
     *
     * @param shortUrl
     * @return
     */
    public User findUserByShorturl(String shortUrl);

    /**
     * 查询用户
     * 
     * @param id 用户ID
     * @return 用户
     */
    public User findUserById(Long id);

    /**
     * 按username查询用户管理详细信息
     *
     * @param username
     * @return
     */
    public User findUserByUsername(String username);

    /**
     * 按手机号码查询用户管理详细信息
     *
     * @param mobile
     * @return
     */
    public User findUserByMobile(String mobile);

    /**
     * 按邮箱查询用户管理详细信息
     *
     * @param email
     * @return
     */
    public User findUserByEmail(String email);

    /**
     * 查询用户数量
     *
     * @param pager 分页处理类
     * @return 用户数量
     */
    public int queryUserTotal(Pager pager);

    /**
     * 查询用户列表
     * 
     * @param pager 分页处理类
     * @return 用户集合
     */
    public List<User> selectUserPager(Pager pager);

    /**
     * 查询需要导出的用户列表
     *
     * @param user 用户信息
     * @return 用户集合
     */
    public List<User> exportUserList(User user);
}
