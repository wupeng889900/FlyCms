package com.flycms.modules.user.controller.manage;

import com.flycms.modules.user.domain.dto.UserDTO;
import com.flycms.modules.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.user.domain.User;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 用户管理Controller
 * 
 * @author admin
 * @date 2020-05-24
 */
@Api("用户信息管理")
@RestController
@RequestMapping("/system/user/user")
public class UserAdminController extends BaseController
{
    @Autowired
    private IUserService userService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增用户管理
     */
    @PreAuthorize("@ss.hasPermi('user:user:add')")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody User user)
    {
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUsernameUnique(user)))
        {
            return AjaxResult.error("新增用户管理'" + user.getUsername() + "'失败，用户名称已存在");
        }
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserMobileUnique(user)))
        {
            return AjaxResult.error("新增用户管理'" + user.getMobile() + "'失败，用户手机号码已存在");
        }
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserEmailUnique(user)))
        {
            return AjaxResult.error("新增用户管理'" + user.getEmail() + "'失败，用户邮箱已存在");
        }
        return toAjax(userService.insertUser(user));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户管理
     */
    @PreAuthorize("@ss.hasPermi('user:user:remove')")
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userService.deleteUserByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户管理
     */
    @PreAuthorize("@ss.hasPermi('user:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody User user)
    {
        return toAjax(userService.updateUser(user));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询用户管理列表
     */
    @ApiOperation("获取用户列表")
    @PreAuthorize("@ss.hasPermi('user:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(User user,
                              @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @Sort @RequestParam(defaultValue = "id") String sort,
                              @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<UserDTO> pager = userService.selectUserPager(user, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出用户管理列表
     */
    @PreAuthorize("@ss.hasPermi('user:user:export')")
    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(User user)
    {
        List<UserDTO> userList = userService.exportUserList(user);
        ExcelUtil<UserDTO> util = new ExcelUtil<UserDTO>(UserDTO.class);
        return util.exportExcel(userList, "用户管理列表");
    }

    /**
     * 获取用户管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('user:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userService.findUserById(id));
    }
}
