package com.flycms.modules.notify.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsSign;
import com.flycms.modules.notify.domain.dto.SmsSignDto;

/**
 * 短信签名Service接口
 * 
 * @author admin
 * @date 2020-05-27
 */
public interface ISmsSignService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信签名
     *
     * @param smsSign 短信签名
     * @return 结果
     */
    public int insertSmsSign(SmsSign smsSign);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信签名
     *
     * @param ids 需要删除的短信签名ID
     * @return 结果
     */
    public int deleteSmsSignByIds(Long[] ids);

    /**
     * 删除短信签名信息
     *
     * @param id 短信签名ID
     * @return 结果
     */
    public int deleteSmsSignById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信签名
     *
     * @param smsSign 短信签名
     * @return 结果
     */
    public int updateSmsSign(SmsSign smsSign);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询短信签名
     * 
     * @param id 短信签名ID
     * @return 短信签名
     */
    public SmsSign selectSmsSignById(Long id);

    /**
     * 查询短信签名列表
     * 
     * @param smsSign 短信签名
     * @return 短信签名集合
     */
    public Pager<SmsSignDto> selectSmsSignPager(SmsSign smsSign, Integer page, Integer limit, String sort, String order);
}
