package com.flycms.framework.aspectj;

import java.lang.reflect.Method;

import com.flycms.common.utils.spring.SpringUtils;
import com.flycms.framework.security.LoginAdmin;
import com.flycms.modules.system.domain.FlyAdmin;
import com.flycms.modules.system.domain.FlyRole;
import com.flycms.common.utils.ServletUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.aspectj.lang.annotation.DataScope;
import com.flycms.framework.web.domain.BaseEntity;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import com.flycms.framework.security.service.TokenService;

/**
 * 数据过滤处理
 * 
 * @author kaifei sun
 */
@Aspect
@Component
public class DataScopeAspect
{
    /**
     * 全部数据权限
     */
    public static final String DATA_SCOPE_ALL = "1";

    /**
     * 自定数据权限
     */
    public static final String DATA_SCOPE_CUSTOM = "2";

    /**
     * 部门数据权限
     */
    public static final String DATA_SCOPE_DEPT = "3";

    /**
     * 部门及以下数据权限
     */
    public static final String DATA_SCOPE_DEPT_AND_CHILD = "4";

    /**
     * 仅本人数据权限
     */
    public static final String DATA_SCOPE_SELF = "5";

    // 配置织入点
    @Pointcut("@annotation(com.flycms.framework.aspectj.lang.annotation.DataScope)")
    public void dataScopePointCut()
    {
    }

    @Before("dataScopePointCut()")
    public void doBefore(JoinPoint point) throws Throwable
    {
        handleDataScope(point);
    }

    protected void handleDataScope(final JoinPoint joinPoint)
    {
        // 获得注解
        DataScope controllerDataScope = getAnnotationLog(joinPoint);
        if (controllerDataScope == null)
        {
            return;
        }
        // 获取当前的用户
        LoginAdmin loginAdmin = SpringUtils.getBean(TokenService.class).getLoginAdmin(ServletUtils.getRequest());
        FlyAdmin currentUser = loginAdmin.getAdmin();
        if (currentUser != null)
        {
            // 如果是超级管理员，则不过滤数据
            if (!currentUser.isAdmin())
            {
                dataScopeFilter(joinPoint, currentUser, controllerDataScope.deptAlias(),
                        controllerDataScope.adminAlias());
            }
        }
    }

    /**
     * 数据范围过滤
     * 
     * @param joinPoint 切点
     * @param admin 用户
     * @param adminAlias 别名
     */
    public static void dataScopeFilter(JoinPoint joinPoint, FlyAdmin admin, String deptAlias, String adminAlias)
    {
        StringBuilder sqlString = new StringBuilder();
        for (FlyRole role : admin.getRoles()){
            String dataScope = role.getDataScope();
            if (DATA_SCOPE_ALL.equals(dataScope))
            {
                sqlString = new StringBuilder();
                break;
            }
            else if (DATA_SCOPE_CUSTOM.equals(dataScope))
            {
                sqlString.append(StrUtils.format(
                        " AND {}.dept_id IN ( SELECT dept_id FROM fly_role_dept WHERE role_id = {} ) ", deptAlias,
                        role.getRoleId()));
            }
            else if (DATA_SCOPE_DEPT.equals(dataScope))
            {
                sqlString.append(StrUtils.format(" OR {}.dept_id = {} ", deptAlias, admin.getDeptId()));
            }
            else if (DATA_SCOPE_DEPT_AND_CHILD.equals(dataScope))
            {
                sqlString.append(StrUtils.format(
                        " AND {}.dept_id IN ( SELECT dept_id FROM fly_dept WHERE dept_id = {} or find_in_set( {} , ancestors ) )",
                        deptAlias, admin.getDeptId(), admin.getDeptId()));
            }
            else if (DATA_SCOPE_SELF.equals(dataScope))
            {
                if (StrUtils.isNotBlank(adminAlias))
                {
                    sqlString.append(StrUtils.format(" AND {}.user_id = {} ", adminAlias, admin.getAdminId()));
                }
                else
                {
                    // 数据权限为仅本人且没有adminAlias别名不查询任何数据
                    sqlString.append(" AND 1=0 ");
                }
            }
        }

        if (StrUtils.isNotBlank(sqlString.toString()))
        {
            BaseEntity baseEntity = (BaseEntity) joinPoint.getArgs()[0];
            baseEntity.setDataScope(" AND (" + sqlString.substring(4) + ")");
        }
    }

    /**
     * 是否存在注解，如果存在就获取
     */
    private DataScope getAnnotationLog(JoinPoint joinPoint)
    {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if (method != null)
        {
            return method.getAnnotation(DataScope.class);
        }
        return null;
    }
}
