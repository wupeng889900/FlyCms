package com.flycms.framework.config;

import com.flycms.modules.common.directive.FollowDirective;
import com.flycms.modules.common.directive.PreNextPageDirective;
import com.flycms.modules.common.directive.TextCutDirective;
import com.flycms.modules.common.directive.TimeFormatDirective;
import com.flycms.modules.data.directive.LabelDirective;
import com.flycms.modules.elastic.directive.SearchDirective;
import com.flycms.modules.group.directive.GroupDirective;
import com.flycms.modules.group.directive.GroupTopicDirective;
import com.flycms.modules.site.directive.LinksDirective;
import com.flycms.modules.user.directive.UserDirective;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Configuration
public class FreeMarkerConfig {

    @Resource
    protected freemarker.template.Configuration configuration;
    @Autowired
    protected SearchDirective searchDirective;
    @Autowired
    protected UserDirective userDirective;
    @Autowired
    protected LinksDirective linksDirective;
    @Autowired
    protected TextCutDirective textCutDirective;
    @Autowired
    protected TimeFormatDirective timeFormatDirective;
    @Autowired
    protected PreNextPageDirective preNextPageDirective;
    @Autowired
    protected LabelDirective labelDirective;
    @Autowired
    protected GroupDirective groupDirective;
    @Autowired
    protected GroupTopicDirective groupTopicDirective;
    @Autowired
    protected FollowDirective followDirective;

    /**
     * 添加自定义标签
     */
    @PostConstruct
    public void setSharedVariable() {
        /*
         * 向freemarker配置中添加共享变量;
         * 它使用Configurable.getObjectWrapper()来包装值，因此在此之前设置对象包装器是很重要的。（即上一步的builder.build().wrap操作）
         * 这种方法不是线程安全的;使用它的限制与那些修改设置值的限制相同。
         * 如果使用这种配置从多个线程运行模板，那么附加的值应该是线程安全的。
         */
        configuration.setSharedVariable("fly_search", searchDirective);
        configuration.setSharedVariable("fly_user", userDirective);
        configuration.setSharedVariable("fly_links", linksDirective);
        //截取内容字符串标签
        configuration.setSharedVariable("text_cut",textCutDirective);
        //格式化时间
        configuration.setSharedVariable("time_format", timeFormatDirective);
        //查询上一页下一页标签
        configuration.setSharedVariable("fly_prenext",preNextPageDirective);
        //标签列表
        configuration.setSharedVariable("fly_label",labelDirective);
        //小组列表标签
        configuration.setSharedVariable("fly_group",groupDirective);
        //小组话题列表标签
        configuration.setSharedVariable("fly_topic",groupTopicDirective);
        //关注查询标签
        configuration.setSharedVariable("fly_follow",followDirective);
    }
}